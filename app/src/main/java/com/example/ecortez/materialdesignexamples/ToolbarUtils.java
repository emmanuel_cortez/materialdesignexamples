package com.example.ecortez.materialdesignexamples;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by ecortez on 9/5/17.
 */

public class ToolbarUtils {

    public static void setToolbar(Toolbar pToolbar, AppCompatActivity pActivity) {
        pActivity.setSupportActionBar(pToolbar);

        DrawerLayout drawer = (DrawerLayout) pActivity.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                pActivity, drawer, pToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }
}
