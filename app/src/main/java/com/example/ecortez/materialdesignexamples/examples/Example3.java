package com.example.ecortez.materialdesignexamples.examples;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ecortez.materialdesignexamples.R;
import com.example.ecortez.materialdesignexamples.ToolbarUtils;

public class Example3 extends Fragment implements AppBarLayout.OnOffsetChangedListener {

    private static final int PERCENTAGE_TO_SHOW_IMAGE = 80;
    private View mFab;
    private int mMaxScrollSize;
    private boolean mIsImageHidden;

    public Example3() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rooView = inflater.inflate(R.layout.fragment_example3, container, false);

        Toolbar toolbar = (Toolbar) rooView.findViewById(R.id.toolbar);
        ToolbarUtils.setToolbar(toolbar, (AppCompatActivity) getActivity());

        AppBarLayout appbar = (AppBarLayout) rooView.findViewById(R.id.appbar);
        appbar.addOnOffsetChangedListener(this);

        mFab = rooView.findViewById(R.id.fab);

        return rooView;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int currentScrollPercentage = (Math.abs(i)) * 100
                / mMaxScrollSize;

        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_IMAGE) {
            if (!mIsImageHidden) {
                mIsImageHidden = true;

                ViewCompat.animate(mFab).scaleY(0).scaleX(0).start();
            }
        }

        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_IMAGE) {
            if (mIsImageHidden) {
                mIsImageHidden = false;
                ViewCompat.animate(mFab).scaleY(1).scaleX(1).start();
            }
        }
    }
}
