package com.example.ecortez.materialdesignexamples.examples;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ecortez.materialdesignexamples.R;
import com.example.ecortez.materialdesignexamples.ToolbarUtils;

public class Example1 extends Fragment {

    public Example1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rooView = inflater.inflate(R.layout.fragment_example1, container, false);

        Toolbar toolbar = (Toolbar) rooView.findViewById(R.id.toolbar);

        ToolbarUtils.setToolbar(toolbar, (AppCompatActivity) getActivity());

        FloatingActionButton fab = (FloatingActionButton) rooView.findViewById(R.id.fab);

        fab.setOnClickListener(v -> {
            Snackbar.make(fab,"You've clicked the Fab", Snackbar.LENGTH_SHORT).show();
        });

        return rooView;
    }


}
