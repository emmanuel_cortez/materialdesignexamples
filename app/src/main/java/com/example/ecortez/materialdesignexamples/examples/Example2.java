package com.example.ecortez.materialdesignexamples.examples;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ecortez.materialdesignexamples.R;
import com.example.ecortez.materialdesignexamples.ToolbarUtils;

public class Example2 extends Fragment {

    public Example2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rooView = inflater.inflate(R.layout.fragment_example2, container, false);

        Toolbar toolbar = (Toolbar) rooView.findViewById(R.id.toolbar);
        ToolbarUtils.setToolbar(toolbar, (AppCompatActivity) getActivity());

        return rooView;
    }


}
